/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { useState } from 'react';
import {
  SafeAreaView,
  Button,
  TextInput,
  View,
  StatusBar,
  Alert,
  Text,
} from 'react-native';


const App: () => React$Node = () => {
  const [show, setShow] = useState(false)
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1, backgroundColor: 'white', alignItems: 'center', justifyContent: 'center' }}>
        <View style={{ justifyContent: 'center', width: '90%', alignItems: 'center', backgroundColor: 'transparent' }}>
          <TextInput style={{ height: 40, width: '100%', borderColor: 'gray', borderWidth: 1 }} accessible={true} testID="username" />
          <TextInput style={{ height: 40, width: '100%', borderColor: 'gray', borderWidth: 1, marginTop: 10 }} accessible={true} testID="password" />
          <Button title="LOGIN" testID="buttonLogin" accessible={true} onPress={() => {
            setShow(!show)
          }} />
          {show && <Text testID="status">LOGGIN</Text>}
        </View>
      </SafeAreaView>
    </>
  );
};

export default App;
