import wd from 'wd';

jasmine.DEFAULT_TIMEOUT_INTERVAL = 60000;
const PORT = 4723;
const CONFIG = {
  automationName: "Appium",
  platformName: "iOS",
  platformVersion: "13.2",
  deviceName: "iPhone 11",
  app: "/Users/lvmtam/development/appcenter_appium/ios/build/appcenter_appium/Build/Products/Debug-iphonesimulator/appcenter_appium.app",
  autoAcceptAlerts: true,
  autoGrantPermissions: true,
  capabilities: { "notifications": "YES" }
}

const driver = wd.promiseChainRemote('localhost', PORT);

const timestamp = () => {
  const d = new Date()
  var yyyy = d.getFullYear().toString();                                    
  var mm = (d.getMonth()+1).toString();      
  var dd  = d.getDate().toString();
  return yyyy + '-' + (mm[1]?mm:"0"+mm[0]) + '-' + (dd[1]?dd:"0"+dd[0]) + ' ' + `${d.getHours()}:${d.getMinutes()}:${d.getSeconds()}:${d.getMilliseconds()}`;
}

beforeAll(async () => {
  await driver.init(CONFIG)
  await driver.sleep(3000)
})

afterAll(async () => {
  console.log('afterAll() done.')
})

it('Test #001: non-testcase', async () => { console.log(timestamp(), '>>>>> Test #001: non-testcase OK')})

it('Test #002: UI', async () => {
  console.log(timestamp(), '>>>>> Test #002: UI be displayed')
  expect(await driver.hasElementByAccessibilityId('buttonLogin')).toBe(true);
})

it('Test #003: click() action', async () => {
  console.log(timestamp(), '>>>>> Test #003: Actions')
  expect(await driver.hasElementByAccessibilityId('buttonLogin')).toBe(true);
  

  const element = await driver.elementByAccessibilityId('buttonLogin')
  await element.click()
})